---
layout: default
permalink: /banner/
title: "banner"
description: "page with a banner layout"
canonical: ""
---
<main>
{%- assign legos = site.legos | where: "page", "banner" -%}
{% for lego in legos %}
  {%- if lego.include != null -%}
    {% include {{ lego.include }} -%}
  {% endif %}
{% endfor %}
</main>