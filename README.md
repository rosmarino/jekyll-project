# Jekyll Project

Empty, ready made Jekyll website with custom style. Live at: https://jekyll-project.onrender.com/

The website wiki explains step-by-step how to build the website by yourself: https://gitlab.com/rosmarino/jekyll-project/-/wikis/home

da sviluppare:
- responsive mobile first
- blazing fast
- full Jekyll power
- open source building blocks (Jekyll, W3S CSS, SASS)
- easily customizable
- easy to deploy free of charge
- blog ready with user comments
- snippets
